gray-monarch.nodejs
=========

Installs NodeJS/npm and optionally updated version from `n`

Requirements
------------

-   None

Role Variables
--------------

##### defaults/main.yml

|            Variable Name | Description                                     | Default                                                       |
|-------------------------:|:------------------------------------------------|:--------------------------------------------------------------|
| `gm_nodejs_node_version` | Version of NodeJS to install as taken by `n`    | `4.5.0`                                                       |
|  `gm_nodejs_n_node_path` | Path to `n` install bin directory               | `/usr/local/n/versions/node/{{ gm_nodejs_node_version }}/bin` |
|        `gm_nodejs_use_n` | Boolean to turn on/off installing node from `n` | `true`                                                        |

##### vars/\[Debian|RedHat\].yml

|              Variable Name | Description                                   | Default                          |
|---------------------------:|:----------------------------------------------|:---------------------------------|
|  `gm_nodejs_dist_dep_pkgs` | List of packages to install before node/npm | RedHat.yml<br />`- epel-release` |
| `gm_nodejs_node_dist_pkgs` | List of packages needed to install node/npm   | `- nodejs`<br />`- npm`          |

Dependencies
------------

-   None

Example Playbook
----------------

Defaults:

    - hosts: servers
      roles:
        - gray-monarch.nodejs


Different NodeJS version provided by `n`:

    - hosts: servers
      roles:
        - role: gray-monarch.nodejs
          gm_nodejs_node_version: 0.10.46

License
-------

Apache 2

Author Information
------------------

Michael Goodwin <mike@contactvelocity.com>
